__author__ = 'Cyb3rnetic'
__copyright__ = 'Copyleft 2017 Cyb3rnetic'
__license__ = 'Open Source'

"""
Reqiured Imports
"""
import sl4a, android, time

"""
Global
"""
droid = android.Android()

"""
Logging

0 = Object Logging
1 = Text Logging
"""
logging = 1
log_file = '/storage/emulated/0/qpython/scripts3/WIFI_GPS_LOG.log'

"""
Colors
"""
W = '\033[0m'     # white (normal)
R = '\033[31m'    # red
G = '\033[32m'    # green
O = '\033[33m'    # orange
B = '\033[34m'    # blue
P = '\033[35m'    # purple
C = '\033[36m'    # cyan


def main():
  """
  main()

  The main function will initiate the scanning process
  """
  print(B + "\n\n######"+W)
  print(B + "#     # #####   ####  # #####"+W)
  print(B + "#     # #    # #    # # #    #"+W)
  print(B + "#     # #    # #    # # #    #"+W)
  print(B + "#     # #####  #    # # #    #"+W)
  print(B + "#     # #   #  #    # # #    #"+W)
  print(B + "######  #    #  ####  # #####\n\n"+W)

  print (B+"********************************\n"+B+"*   "+O+"Android WIFI  GPS Logger   "+B+"*\n"+B+"********************************\n"+W)

  time.sleep(4)

  networks = {}

  while True:
    droid.startLocating()

    while not droid.readLocation().result:
      print(R+"Waiting for GPS link...")
      time.sleep(10)

    event = droid.readLocation()

    while not droid.wifiStartScan().result:
      print(R + "Waiting for WIFI scan...")
      time.sleep(5)
    for ap in droid.wifiGetScanResults().result:
      if ap['bssid'] not in networks:
        print(G + "Found new network! ["+ap['ssid']+"]")
        networks[ap['bssid']] = ap.copy()

        if 'gps' in event.result:
          networks[ap['bssid']]['lat'] = str(event.result['gps']['latitude'])
          networks[ap['bssid']]['lng'] = str(event.result['gps']['longitude'])

        log = open(log_file, 'a')

        if logging is 0:
          log.write(str(networks[ap['bssid']]) + "\n")
        elif logging is 1:
          log.write("SSID: " + ap['ssid'] + "\n")
          log.write("BSSID: " + ap['bssid'] + "\n")
          log.write("LEVEL: " + str(ap['level']) + "\n")
          log.write("CAPABILITIES: " + ap['capabilities'] + "\n")
          log.write("FREQUENCY: " + str(ap['frequency']) + "\n")
          log.write("LAT: " + str(networks[ap['bssid']]['lat']) + "\n")
          log.write("LNG: " + str(networks[ap['bssid']]['lat']) + "\n\n\n")

        log.close()

if __name__ == "__main__":
 main ()